package com.matias.bouncingbullets;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.viewport.FitViewport;

import static com.matias.bouncingbullets.Main.textureAtlas;


public class MapSelectionScreen extends BaseScreen{

    private final float WORLD_HEIGHT = Gdx.graphics.getHeight();
    private final float WORLD_WIDTH = WORLD_HEIGHT*Main.ASPECT_RATIO;

    private Array<Mapa> mapas;
    private Skin skin;

    /**
     * Constructor del modelo base de las pantallas del juego.
     *
     * @param parent Juego al que pertenece.
     */
    public MapSelectionScreen(Game parent, Skin skin) {
        super(parent);
        this.skin = skin;
    }

    @Override
    public void show() {
        setStage(new Stage(new FitViewport(WORLD_WIDTH,WORLD_HEIGHT)));
        this.mapas = new Array<Mapa>();

        try{
            System.err.println(Gdx.files.getLocalStoragePath());
            FileHandle[] jsonFiles = Gdx.files.internal(Gdx.files.getLocalStoragePath()).list("json");

            for(int i = 0;i < jsonFiles.length;i++){
                cargarMapa(jsonFiles[i]);
                TextButton b = new TextButton("Nombre de mapa: "+mapas.get(i).nombre+"\n Dificultad: "+mapas.get(i).dificultad+"\n Número de balas:"+mapas.get(i).maxBalas,this.skin);
                getStage().addActor(b);
            }

        }catch(GdxRuntimeException e){
            System.err.println(e.getLocalizedMessage());
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        getStage().act();
        getStage().draw();
    }

    /**
     * Carga el mapa que se desea jugar.
     */
    private void cargarMapa(FileHandle jsonFile) {
        if(jsonFile.exists()){
            Json json = new Json();
            this.mapas.add(json.fromJson(Mapa.class,jsonFile));
        }
    }
}
