package com.matias.bouncingbullets;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Cantenedor de datos para cargar los mapas desde el archivo que se le pasa a la pantalla principal del juego.
 */
public class Mapa {

    /**
     * Nombre del mapa.
     */
    public String nombre;

    /**
     * Dificultad del mapa.
     */
    public String dificultad;

    /**
     * Número máxmimo de balas de la pantalla.
     */
    public int maxBalas;

    /**
     * Componen R del color de fondo definido en RGB.
     */
    public float R;

    /**
     * Componen G del color de fondo definido en RGB.
     */
    public float G;

    /**
     * Componen B del color de fondo definido en RGB.
     */
    public float B;

    /**
     * Punto de aparición del jugador.
     */
    public Vector2 jugadorSpawn;

    /**
     * Puntos de aparición para los bonificadores.
     */
    public Array<Vector2> powerUpSpawnPoints;

    /**
     * Puntos de aparición para las balas.
     */
    public Array<Vector2> balasSpawn;

    /**
     * Posiciones de las rocas de la pantalla
     */
    public Array<Vector2> rocas;

    public Mapa() {
    }
}
