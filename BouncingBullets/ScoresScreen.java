package com.matias.bouncingbullets;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ScoresScreen extends BaseScreen{

    private final float WORLD_HEIGHT = Gdx.graphics.getHeight();
    private final float WORLD_WIDTH = WORLD_HEIGHT*Main.ASPECT_RATIO;

    private Table tabla;
    private ScrollPane scrollPane;
    private String puntuacionesStr = "";
    private Array<Mapa> mapas;
    private Skin skin;

    /**
     * Constructor del modelo base de las pantallas del juego.
     *
     * @param parent Juego al que pertenece.
     */
    public ScoresScreen(Game parent, Skin skin) {
        super(parent);
        this.skin = skin;
    }

    @Override
    public void show() {
        setStage(new Stage(new FitViewport(WORLD_WIDTH,WORLD_HEIGHT)));
        this.tabla = new Table();
        this.tabla.setFillParent(true);
        this.scrollPane = new ScrollPane(this.tabla,this.skin);
        this.scrollPane.setFillParent(true);

        this.tabla.setDebug(true);

        leerPuntuaciones();

        Label l1 = new Label(Main.idioma.get("mapas"),this.skin);
        l1.setWrap(true);

        this.tabla.add(l1).width(WORLD_WIDTH/2).center();
        l1 = new Label(Main.idioma.get("puntuaciones"),this.skin);
        l1.setWrap(true);
        this.tabla.add(l1).width(WORLD_WIDTH/2).center();
        this.tabla.row();

        String[] puntuaciones = this.puntuacionesStr.split(",");
        for (String str: puntuaciones) {
            if(str.contains(";")){
                String[] puntuacion = str.split(";");
                Label l = new Label(puntuacion[0],this.skin);
                Label l2 = new Label(puntuacion[1],this.skin);
                this.tabla.add(l).width(WORLD_WIDTH/2);
                this.tabla.add(l2).width(WORLD_WIDTH/2);
            }else{
                Label l = new Label("",this.skin);
                Label l2 = new Label(str,this.skin);
                this.tabla.add(l).width(WORLD_WIDTH/2);
                this.tabla.add(l2).width(WORLD_WIDTH/2);
            }
            this.tabla.row();
        }

        getStage().addActor(this.scrollPane);
    }

    /**
     * Lee el archivo de puntuaciones.
     */
    private void leerPuntuaciones() {
        FileHandle intputRecords = Gdx.files.internal("records.txt");
        System.err.println(intputRecords.exists() || intputRecords.length() > 0);
        if(intputRecords.exists()) {
            try {
                InputStream in = intputRecords.read();
                BufferedInputStream input = new BufferedInputStream(in);
                int leido = 0;
                while ((leido = input.read()) != -1) {
                    puntuacionesStr += (char) leido;
                }
            } catch (GdxRuntimeException | IOException e) {
                Main.platformSpecific.avisarUsuario(Main.idioma.get("errorarchivoleer"));
            }
        }else{
            Label a = new Label(Main.idioma.get("nopuntuaciones"),this.skin);
            a.setWrap(true);
            tabla.add(a).height(WORLD_HEIGHT).width(WORLD_WIDTH);
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        getStage().act();
        getStage().draw();
    }

    @Override
    public void resize(int width, int height) {
        getStage().getViewport().update(width,height);
    }
}
