package com.matias.bouncingbullets;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Lanza el juego en android
 */
public class AndroidLauncher extends AndroidApplication {

	/**
	 * Nombre del canal de notificaciones.
	 */
	private static final String CHANNEL_ID = "Bouncing Bullets";

	/**
	 * Archivo al que se guardan las fotos tomadas.
	 */
	public static File fotoArchivo;

	/**
	 * Comprueba los permisos necesarios para el juego.
	 */
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		comprobarPermiso(new String[] {Manifest.permission.CAMERA,Manifest.permission.VIBRATE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},3);
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useImmersiveMode = true;
		config.useWakelock = true;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			initialize(new Main(new CamaraAndroid(this)),config);
		}
	}

	/**
	 * Comprueba el acceso a un permiso dado, si no se puede acceder a dicho permiso solicita acceso.
	 * @param permiso Permiso a comprobar.
	 * @param requestCode Código de petición de la comprobación.
	 */
	private void comprobarPermiso(String[] permiso,int requestCode) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			requestPermissions(permiso, requestCode);
		}
	}

	/**
	 * Guarda en el almacenamiento designado a la aplicación la imagen tomada por la camara.
	 * @param requestCode Código de la petición.
	 * @param resultCode Si el usuario a cerrado la actividad satisfecho o no.
	 * @param data Información, en este caso la imagen.
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1 && resultCode == RESULT_OK) { // Thumbnail
			Bitmap foto = BitmapFactory.decodeFile(AndroidLauncher.fotoArchivo.getAbsolutePath());
			try {
				FileOutputStream out = openFileOutput(AndroidLauncher.fotoArchivo.getName().replace(".png",".jpeg"), Context.MODE_PRIVATE);
				BufferedOutputStream output = new BufferedOutputStream(out);
				foto.compress(Bitmap.CompressFormat.JPEG,70,output);

				output.close();
				out.close();
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
					CharSequence name = "Bouncing Bullets";
					int importance = NotificationManager.IMPORTANCE_DEFAULT;
					NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
					channel.enableLights(true);
					channel.enableVibration(true);
					NotificationManager notificationManager = getSystemService(NotificationManager.class);
					notificationManager.createNotificationChannel(channel);
					Notification.Builder builder = new Notification.Builder(this,CHANNEL_ID);
					builder.setSmallIcon(Icon.createWithBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.icono2)));
					builder.setContentText(getResources().getText(R.string.notificacion));
					notificationManager.notify(1,builder.build());
				}
				AndroidLauncher.fotoArchivo.deleteOnExit();
			} catch (IOException e) {
				System.err.println(e.getLocalizedMessage());
				Main.platformSpecific.avisarUsuario(Main.idioma.get("errorfoto"));
			}
		}
	}
}
