package com.matias.bouncingbullets;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Base para las pantallas del juego.
 */
public abstract class BaseScreen extends ScreenAdapter {

    /**
     * Juego al que pertenece la pantalla.
     */
    private Game parent;

    /**
     * Stage de la pantalla.
     */
    private Stage stage;

    /**
     * Constructor del modelo base de las pantallas del juego.
     * @param parent Juego al que pertenece.
     */
    public BaseScreen(Game parent) {
        this.parent = parent;
    }

    /**
     * Devuelve el juego al que pertenece la pantalla.
     * @return Referencia al juego al que peretnece la pantalla.
     */
    public Game getParent() {
        return parent;
    }

    /**
     * Devuelve el stage de la pantalla.
     * @return Stage de la pantalla.
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * Establece el stage de la pantalla.
     * @param stage Stage que se quiere utilizar en la pantalla.
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
