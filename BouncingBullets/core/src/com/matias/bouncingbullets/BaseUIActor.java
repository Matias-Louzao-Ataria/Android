package com.matias.bouncingbullets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Base de los actores de interfaz de usuario
 */
public class BaseUIActor extends Actor {

    /**
     * Textura del actor de interfaz de usuario.
     */
    protected  TextureRegion texture;

    /**
     * Anchura del actor de interfaz de usuario.
     */
    public static float WIDTH = 2f;

    /**
     * Anchura del actor de interfaz de usuario.
     */
    public static float HEIGHT = 1.8f;

    /**
     * Tipo de powerUp que representa.
     */
    private PowerUpObject.TipoObj tipo = null;

    /**
     * Constructor de BaseUIActor
     * @param texture Textura del actor de interfaz de usuario.
     * @param x Posición en X.
     * @param y Posición en Y.
     */
    public BaseUIActor(TextureRegion texture, float x, float y){
        this.texture = texture;
        this.setPosition(x,y);
        this.setSize(WIDTH,HEIGHT);
    }

    /**
     * Constructor de BaseUIActor
     * @param texture Textura del actor de interfaz de usuario.
     * @param x Posición en X.
     * @param y Posición en Y.
     * @param tipo Tipo de powerUp que representa.
     */
    public BaseUIActor(TextureRegion texture,float x, float y, PowerUpObject.TipoObj tipo){
        this(texture,x,y);
        this.tipo = tipo;
    }

    /**
     * Dibuja el actor
     * @param batch Batch para dibujar el actor.
     * @param parentAlpha Transparencia del actor.
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(this.texture,this.getX(),this.getY(),getWidth(),getHeight());
    }

    /**
     * Devuelve el tipo de powerUp que representa
     * @return Tipo de powerUp
     */
    public PowerUpObject.TipoObj getTipo() {
        return tipo;
    }

    /**
     * Libera los recursos.
     */
    public void dispose(){
        this.remove();
    }
}
