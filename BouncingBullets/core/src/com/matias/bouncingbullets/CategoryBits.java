package com.matias.bouncingbullets;

/**
 * Filtros de JBox2D
 */
public class CategoryBits {

    /**
     * Filtro del jugador para el motor de Box2D
     */
    public static final short JUGADOR = 1;

    /**
     * Filtro de las paredes para el motor de Box2D
     */
    public static final short PARED = 2;

    /**
     * Filtro de las balas para el motor de Box2D
     */
    public static final short BALA = 3;

    /**
     * Filtro de las rocas para el motor de Box2D
     */
    public static final short ROCA = 4;

    /**
     * Filtro de los powerUps en el mapa para el motor de Box2D
     */
    public static final short OBJETO = 5;
}
