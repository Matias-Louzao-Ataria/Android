package com.matias.bouncingbullets;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Pantalla que contiene la leyenda de los objetos y actores del juego, además del tutorial de movimiento.
 */
public class Leyenda extends BaseScreen {

    /**
     * Altura del stage
     */
    private final float HEIGHT = Gdx.graphics.getHeight();

    /**
     * Anchura del stage
     */
    private final float WIDTH = HEIGHT*Main.ASPECT_RATIO;

    /**
     * Skin a usar
     */
    private Skin skin;

    /**
     * Tabla que contendrá elementos.
     */
    private Table table;

    /**
     * Scroll para poder ver elementos no visibles a primera vista
     */
    private ScrollPane scrollPane;

    /**
     * Indica si esta pantalla está formando parte de la presentación
     */
    private boolean first = false;

    /**
     * Mapa a cargar la primera que se juega.
     */
    private Mapa mapa;

    /**
     * Imagen seleccionada.
     */
    private Texture selectedImage;

    /**
     * Constructor del modelo base de las pantallas del juego.
     *
     * @param parent Juego al que pertenece.
     * @param skin Aspecto de la interfaz de usuario de la pantalla.
     */
    public Leyenda(Game parent,Skin skin) {
        super(parent);
        this.skin = skin;
    }

    /**
     * Constructor del modelo base de las pantallas del juego.
     * @param parent Juego al que pertenece.
     * @param skin Aspecto de la interfaz de usuario de la pantalla.
     * @param mapa Mapa a cargar la primera que se juega.
     * @param imagenFondo Imagen seleccionada.
     */
    public Leyenda(Game parent, Skin skin, Mapa mapa, Texture imagenFondo) {
        this(parent,skin);
        this.mapa = mapa;
        this.selectedImage = imagenFondo;
    }

    /**
     * Constructor del modelo base de las pantallas del juego.
     * @param parent Juego al que pertenece.
     * @param skin Aspecto de la interfaz de usuario de la pantalla.
     * @param mapa Mapa a cargar la primera que se juega.
     */
    public Leyenda(Game parent, Skin skin, Mapa mapa) {
        this(parent,skin);
        this.mapa = mapa;
    }

    /**
     * Constructor del modelo base de las pantallas del juego.
     * @param parent Juego al que pertenece.
     * @param skin Aspecto de la interfaz de usuario de la pantalla.
     * @param first Indica si esta pantalla está formando parte de la presentación
     * @param mapa Mapa a cargar la primera que se juega.
     * @param imagenFondo Imagen seleccionada.
     */
    public Leyenda(Game parent, Skin skin, boolean first, Mapa mapa, Texture imagenFondo) {
        this(parent, skin, mapa, imagenFondo);
        this.first = first;
    }

    /**
     * Constructor del modelo base de las pantallas del juego.
     * @param parent Juego al que pertenece.
     * @param skin Aspecto de la interfaz de usuario de la pantalla.
     * @param mapa Mapa a cargar la primera que se juega.
     */
    public Leyenda(Game parent, boolean first, Skin skin, Mapa mapa) {
        this(parent, skin, mapa);
        this.first = first;
    }


    /**
     *Inicializa los componentes de la interfaz de usuario de la pantalla y los coloca de la forma designada.
     */
    @Override
    public void show(){
        setStage(new Stage(new FitViewport(WIDTH,HEIGHT)));

        Button btnSalir = new TextButton(Main.idioma.get("entendido"),this.skin);
        btnSalir.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(first){
                    try{
                        DataOutputStream output = new DataOutputStream(Gdx.files.local("first").write(false));

                        output.close();
                    }catch(GdxRuntimeException | IOException e){
                        System.err.println(e.getLocalizedMessage());
                        Main.platformSpecific.avisarUsuario(Main.idioma.get("errorarchivo"));
                    }
                    Screen pantalla = null;
                    if(selectedImage != null){
                        pantalla = new MainGameScreen(getParent(),mapa,selectedImage);
                    }else{
                        pantalla =  new MainGameScreen(getParent(),mapa);
                    }
                    final Screen temp = pantalla;
                    getStage().addAction(Actions.sequence(Actions.fadeOut(1f),Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            getParent().setScreen(temp);
                        }
                    })));
                }else{
                    getStage().addAction(Actions.sequence(Actions.fadeOut(1f),Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            getParent().setScreen(new MainMenuScreen(getParent()));
                        }
                    })));
                }
            }
        });

        btnSalir.setSize(WIDTH,HEIGHT*0.25f/2);
        btnSalir.setPosition(0,HEIGHT);
        getStage().addActor(btnSalir);

        btnSalir.addAction(Actions.moveBy(0,-btnSalir.getHeight(),1.25f));

        this.table = new Table();
        this.table.setFillParent(false);
        this.scrollPane = new ScrollPane(this.table,this.skin);
        this.scrollPane.setSize(WIDTH,HEIGHT*2f/4f);
        this.scrollPane.setPosition(0,HEIGHT -btnSalir.getHeight() - this.scrollPane.getHeight());
        this.scrollPane.setFillParent(false);
        this.scrollPane.setScrollingDisabled(true,false);

        Image imagen = new Image(((Main)getParent()).textureAtlas.findRegion("piedra"));
        Label lbl = crearLabel(Main.idioma.get("roca"));
        agregar(imagen, lbl);
        this.table.row();

        lbl = crearLabel(Main.idioma.get("bala"));
        imagen = new Image(((Main)getParent()).textureAtlas.findRegion("bala"));
        agregar(imagen,lbl);
        this.table.row();

        lbl = crearLabel(Main.idioma.get("pollo"));
        imagen = new Image(((Main)getParent()).textureAtlas.findRegion("pollo"));
        agregar(imagen,lbl);
        this.table.row();

        lbl = crearLabel(Main.idioma.get("chaleco"));
        imagen = new Image(((Main)getParent()).textureAtlas.findRegion("chaleco"));
        agregar(imagen,lbl);
        this.table.row();

        lbl = crearLabel(Main.idioma.get("boton"));
        imagen = new Image(((Main)getParent()).textureAtlas.findRegion("boton2"));
        agregar(imagen,lbl);
        this.table.row();


        Table table = new Table();
        table.setSize(WIDTH,HEIGHT*1.5f/4f);
        lbl = crearLabel(Main.idioma.get("movimiento"));
        lbl.setWrap(true);
        table.add(lbl).size(WIDTH/2f,HEIGHT*1.5f/4f);
        lbl = crearLabel(Main.idioma.get("powerup"));
        lbl.setWrap(true);
        table.add(lbl).size(WIDTH/2f,HEIGHT*1.5f/4f);;

        table.setPosition(0,-table.getHeight());
        table.addAction(Actions.moveBy(0,table.getHeight(),1.25f));

        getStage().addActor(table);

        getStage().addActor(this.scrollPane);
        Gdx.input.setInputProcessor(getStage());

        this.table.addAction(Actions.sequence(Actions.alpha(0),Actions.fadeIn(1.25f)));
    }

    /**
     * Añade a la tabla una imagen con su correspondiente etiqueta
     * @param imagen Imagen a añadir
     * @param lbl Etiqueta a añadir
     */
    private void agregar(Image imagen, Label lbl) {
        float imgAspectRatio = imagen.getHeight()/ imagen.getWidth();
        this.table.add(imagen).width(WIDTH/3f).height((WIDTH/3f) * imgAspectRatio).center();
        this.table.add(lbl).width(WIDTH*2f/3f).height((WIDTH/3f) * imgAspectRatio).center();
    }

    /**
     * Crea una etiqueta
     * @param str Texto de la etiqueta
     * @return Devuelve la etiqueta creada
     */
    public Label crearLabel(String str){
        Label.LabelStyle style = new Label.LabelStyle();
        style.font = Main.bitmapFont;
        style.font.getData().setScale(2f,2f);
        /*BitmapFont ausiliar = Main.bitmapFont;
        ausiliar.getData().setScale(4f,4f);
        style.font = ausiliar;*/
        Label lbl = new Label(str, style);
        lbl.setWrap(true);
        return lbl;
    }

    /**
     * Renderiza la pantalla
     * @param delta Tiempo que ha pasado desde el último fotograma.
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        getStage().act();
        getStage().draw();
    }

    /**
     * Libera los recursos de la pantalla.
     */
    @Override
    public void dispose() {
        super.dispose();
    }
}
