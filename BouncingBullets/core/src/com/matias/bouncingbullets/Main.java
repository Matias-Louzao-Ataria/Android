package com.matias.bouncingbullets;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.I18NBundle;

import java.util.Locale;

/**
 * Juego principal
 */
public class Main extends Game {
    /**
     * Relación de aspecto con la que se mostrará el juego.
     */
    public static float ASPECT_RATIO = 16/9f;

    /**
     * Interfaz que proporciona acceso a funciones específicas de una plataforma.
     */
    public static PlatformSpecific platformSpecific;

    /**
     * Atlas de texturas con las texturas que se utilizarán en el juego.
     */
    public TextureAtlas textureAtlas;

    /**
     * Asset manager del juego que gestiona los recursos necesarios.
     */
    public AssetManager manager;

    /**
     * Herramienta de libgdx que se usará para la localización del programa.
     */
    public static I18NBundle idioma;

    /**
     * Fuente que se usará en los menús.
     */
    public static BitmapFont bitmapFont;

    /**
     * Determina si se han de mostrar los fotogramas por segundo
     */
    public static boolean fps = false;

    /**
     * Determina si se debe utilizar la vibración.
     */
    public static boolean desactivarVibracion = false;

    /**
     * Determina si debe sonar la música
     */
    public static boolean desactivarMusica = false;

    /**
     * Determina si es la primera vez que se abre el juego.
     */
    public static boolean isFirts = true;


    /**
     * Constructor de la clase Main que inicializa la interfaz para tener acceso a las funciones de cada plataforma.
     * @param platformSpecific Interfaz que proporciona acceso a funciones específicas de una plataforma.
     */
     public Main(PlatformSpecific platformSpecific) {
        Main.platformSpecific = platformSpecific;
     }

    /**
     * Realiza las operaciones de inicialización del juego.
     */
    @Override
    public void create() {
        FileHandle archivo = Gdx.files.internal("Localization");
        idioma = I18NBundle.createBundle(archivo,Locale.getDefault());
        Main.bitmapFont = new BitmapFont(Gdx.files.internal("fuente.fnt"));
        textureAtlas = new TextureAtlas(Gdx.files.internal("GameAssets.atlas"));
        manager = new AssetManager();
        manager.load("bensound/creditos.mp3", Music.class);
        manager.load("bensound/gameplay.mp3", Music.class);
        manager.load("bensound/menuprincipal.mp3", Music.class);
        manager.finishLoading();
        Main.isFirts = !Gdx.files.local("first").exists();
        setScreen(new SplashScreen(this));
    }

    /**
     * Reproduce la música indicada si la música no está desactivada.
     * @param musica Música a reproducir.
     */
    public void reproducirMusica(Music musica){
        if(!Main.desactivarMusica){
            musica.play();
        }
    }

    /**
     * Libera los recusos utilizados cuando no son necesarios.
     */
    @Override
    public void dispose() {
        super.dispose();
        manager.dispose();
        bitmapFont.dispose();
        textureAtlas.dispose();
    }
}
