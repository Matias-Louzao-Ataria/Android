package com.matias.bouncingbullets;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;

/**
 * Pantalla para la presentación del objetivo del juego.
 */
public class PresentationScreen extends BaseScreen{

    /**
     * Tabla de la pantalla.
     */
    private Table table;

    /**
     * Label de la pantalla.
     */
    private Label lbl;

    /**
     * Aspecto para la siguiente pantalla
     */
    private Skin skin;

    /**
     * Mapa a cargar.
     */
    private Mapa mapa;

    /**
     * Imagen a usar de fondo.
     */
    private Texture imagenFondo;

    /**
     * Constructor de PresentationScreen.
     * @param parent Juego al que pertenece.
     */
    public PresentationScreen(Game parent) {
        super(parent);
    }

    /**
     * Constructor de PresentationScreen.
     * @param parent Juego al que pertenece.
     * @param skin Aspecto para la siguiente pantalla.
     */
    public PresentationScreen(Game parent,Skin skin,Mapa mapa) {
        super(parent);
        this.skin = skin;
        this.mapa = mapa;
    }

    /**
     * Constructor de PresentationScreen.
     * @param parent Juego al que pertenece.
     * @param skin Aspecto para la siguiente pantalla.
     * @param mapa Mapa a cargar en la pantalla de juego.
     * @param imagenFondo Imagen a usar de fondo en la pantalla de juego.
     */
    public PresentationScreen(Game parent, Skin skin, Mapa mapa, Texture imagenFondo) {
        this(parent,skin,mapa);
        this.imagenFondo = imagenFondo;
    }

    /**
     * Inicializa los componentes necesarios para la pantalla.
     */
    @Override
    public void show() {
        setStage(new Stage(new FitViewport(Gdx.graphics.getHeight()*Main.ASPECT_RATIO,Gdx.graphics.getHeight())));
        this.table = new Table();
        this.table.setFillParent(true);
        Label.LabelStyle style = new Label.LabelStyle();
        style.font = Main.bitmapFont;
        this.lbl = new Label(Main.idioma.get("presentacion1"),style);
        this.table.addAction(Actions.sequence(
                Actions.alpha(0),
                Actions.fadeIn(1f),
                Actions.delay(1.5f),
                Actions.fadeOut(1f),
                Actions.run(new Runnable() {
            @Override
            public void run() {
                lbl.setText(Main.idioma.get("presentacion2"));
            }
        }),
                Actions.fadeIn(1f),
                Actions.delay(1.5f),
                Actions.fadeOut(1f),
                Actions.run(new Runnable() {
            @Override
            public void run() {
                lbl.setText(Main.idioma.get("presentacion3"));
            }
        }),
                Actions.fadeIn(1f),
                Actions.delay(1.5f),
                Actions.fadeOut(1f),
                Actions.run(new Runnable() {
            @Override
            public void run() {
                lbl.setText(Main.idioma.get("presentacion4"));
            }
        }),
                Actions.fadeIn(1f),
                Actions.delay(1.5f),
                Actions.fadeOut(1f),
                Actions.run(new Runnable() {
            @Override
            public void run() {
                Screen pantalla = null;
                if(imagenFondo != null){
                    pantalla = new Leyenda(getParent(),skin,Main.isFirts,mapa,imagenFondo);
                }else{
                    pantalla = new Leyenda(getParent(),Main.isFirts,skin,mapa);
                }
                getParent().setScreen(pantalla);
            }
        })));
        this.lbl.setFontScale(3f,3f);
        this.table.add(this.lbl);
        getStage().addActor(this.table);
        Gdx.input.setInputProcessor(getStage());
    }

    /**
     * Renderiza la escena de está pantalla.
     * @param delta Tiempo que ha transcurrido desde el último frame.
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        getStage().act();
        getStage().draw();
    }

    /**
     * Gestiona los cambios de tamaño de la pantalla.
     * @param width Nuevo ancho.
     * @param height Nuevo alto.
     */
    @Override
    public void resize(int width, int height) {
        getStage().getViewport().update(width,height);
    }

    /**
     * Libera los recursos de la pantalla.
     */
    @Override
    public void dispose() {
        getStage().dispose();
    }
}
