package com.matias.bouncingbullets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Actor roca.
 */
public class RocaBox2D extends BaseActor {

    /**
     * Posición de la roca en el mapa.
     */
    private Vector2 posicion;

    /**
     * Constructor vacio de RocaBox2D
     */
    public RocaBox2D() {

    }

    /**
     * Constructor de RocaBox2D que genera y establece todo lo necesario.
     * @param world Mundo de la roca.
     * @param texture Textura de la roca.
     * @param posicion Posición de la roca.
     */
    public RocaBox2D(World world, TextureRegion texture, Vector2 posicion) {
        super(world,texture);

        setWIDTH(1f);
        setHEIGHT(1f);

        BodyDef bodyDef = new BodyDef();
        this.posicion = posicion;
        bodyDef.position.set(this.posicion);
        bodyDef.type = BodyDef.BodyType.StaticBody;
        this.body = this.world.createBody(bodyDef);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = 1;
        fixtureDef.filter.categoryBits = CategoryBits.ROCA;
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(getWIDTH(), getHEIGHT());
        fixtureDef.shape = shape;
        this.fixture = this.body.createFixture(fixtureDef);
        this.fixture.setUserData("roca");
        shape.dispose();
        this.setSize(getWIDTH() *2, getHEIGHT() *2);
    }

    /**
     * Dibuja la roca
     * @param batch Batch para dibujar el actor.
     * @param parentAlpha Transparencia del actor.
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        this.setPosition(this.body.getPosition().x- getWIDTH(),this.body.getPosition().y- getHEIGHT());
        batch.draw(this.texture,getX(),getY(),getWidth(),getHeight());
    }

    /**
     * Devuelve el cuerpo de la roca.
     * @return El cuerpo de la roca.
     */
    public Body getBody() {
        return body;
    }

    /**
     * Libera los recursos de la roca.
     */
    public void dispose(){
        super.dispose();
    }
}