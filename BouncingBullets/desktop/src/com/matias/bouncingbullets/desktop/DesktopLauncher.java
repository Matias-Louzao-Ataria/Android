package com.matias.bouncingbullets.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.matias.bouncingbullets.Main;
import com.matias.bouncingbullets.PlatformSpecific;


/**
 * Lanza el juego en escritorio
 */
public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new Main(new PlatformSpecific() {
			@Override
			public void hacerFoto() {
				System.out.println("Not supported");
			}

			public void abrirArchivo(){
				System.out.println("Not supported");
			}

			public void avisarUsuario(String msg){
				System.out.println("Not supported");
			}

			public void preguntarUsuario(String positivo,String negativo,String neutral){
				System.out.println("Not supported");
			}

			@Override
			public void vibrar(int miliseconds) {
				System.out.println("Not supported");
			}

			@Override
			public void vibrar(long[] patern, int repeat) {
				System.out.println("Not supported");
			}
		}), config);
	}
}
